# Devops Simulation

## This repository are for testing kubernetes

This microservice will send the csv with number and do a average of all number, you can see on the [sample.csv](assets/sample.csv)

### Repository List

- [express-ui](https://gitlab.com/devops-simulation/express-ui-form) this is the ui part
- [python-microservice](https://gitlab.com/devops-simulation/py-flask) this is the microservice part

### Todo

- install the docker desktop and enable the kubernetes on settings
- `kubectl apply -f flask-deployment-k8s.yml`
- goto the [express-ui](https://gitlab.com/devops-simulation/express-ui-form) and do the same `kubectl apply -f express-ui-k8s-deploy.yml`


### Sample Output

![Sample Result](assets/output.gif)
